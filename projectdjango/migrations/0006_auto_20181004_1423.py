# Generated by Django 2.1.1 on 2018-10-04 07:23

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projectdjango', '0005_auto_20181004_1421'),
    ]

    operations = [
        migrations.AlterField(
            model_name='jadwal_pribadi',
            name='hari',
            field=models.DateTimeField(max_length=100),
        ),
    ]
