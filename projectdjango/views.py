from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.http import HttpResponse
from .forms import Jadwal_Form
from .models import Jadwal_Pribadi
from django.urls import reverse
from datetime import datetime


# Create your views here.

def page1(request):
    response = {}
    return render(request, 'page1story4.html', response)

def page2(request):
    response = {}
    return render(request, 'page2story4.html', response)

def page3(request):
    response = {}
    return render(request, 'page3story4.html', response)

def page4(request):
    response = {}
    return render(request, 'page4story4.html', response)

def page5form(request):
    response = {}
    response['form_jadwal'] = Jadwal_Form
    return render(request, '5FormJadwal.html', response)

def page5post(request):
    response = {}
    form = Jadwal_Form(request.POST or None)
    if(request.method == 'POST' or form.is_valid()):
        response['hari'] = datetime.strptime(request.POST['hari'], '%Y-%m-%d')
        response['waktu'] = datetime.strptime(request.POST['waktu'], '%H:%M')
        response['namaKegiatan'] = request.POST['namaKegiatan']
        response['tempat'] = request.POST['tempat'] 
        response['kategori'] = request.POST['kategori'] 
        
        jadwal = Jadwal_Pribadi(hari=response['hari'], waktu=response['waktu'],
                          namaKegiatan=response['namaKegiatan'], tempat=response['tempat'],
                          kategori=response['kategori'])
        jadwal.save()
        jadwal_pribadi = Jadwal_Pribadi.objects.all().values()
        html ='5JadwalKegiatan.html'
        return render(request, html, response)
    else:        
        return HttpResponseRedirect(reverse('page5form'))

def page5schedule(request):
    response = {}
    response['jadwal'] = Jadwal_Pribadi.objects.all()
    return render(request, '5JadwalKegiatan.html', response)

def deleteSchedule(request):
    response = {}
    Jadwal_Pribadi.objects.all().delete()
    return HttpResponseRedirect('page5form')
