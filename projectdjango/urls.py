"""projectdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import re_path
from .views import *


urlpatterns = [
    re_path(r'^page1', page1, name='page1'),
    re_path(r'^page2', page2, name='page2'),
    re_path(r'^page3', page3, name='page3'),
    re_path(r'^page4', page4, name='page4'),
    re_path(r'^page5form', page5form, name='page5form'),
    re_path(r'^page5post', page5post, name='page5post'),
    re_path(r'^page5schedule', page5schedule, name='page5schedule'),
    re_path(r'^deleteSchedule', deleteSchedule, name='deleteSchedule'),
    
]
