from django import forms

class Jadwal_Form(forms.Form):
    hari = forms.DateTimeField(label='Hari/Tanggal', required=True, widget=forms.DateInput(attrs={'type':'date'}))
    waktu = forms.TimeField(label='Waktu', required=True, widget=forms.DateInput(attrs={'type':'time'}))
    namaKegiatan = forms.CharField(label='Nama Kegiatan', required=True)
    tempat = forms.CharField(label='Tempat', required=True)
    kategori = forms.CharField(label='Kategori', required=True)

