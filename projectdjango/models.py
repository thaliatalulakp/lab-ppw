from django.db import models
from django.utils import timezone
from datetime import datetime, date

# Create your models here.

class Jadwal_Pribadi(models.Model):
    hari = models.DateTimeField(max_length=100)
    waktu = models.TimeField()
    namaKegiatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)
