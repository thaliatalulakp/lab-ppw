"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include
from django.urls import re_path
from django.contrib import admin
from lab_1.views import index as index_lab1
from projectdjango.views import page1
from projectdjango.views import page2
from projectdjango.views import page3
from projectdjango.views import page4
from projectdjango.views import page5form
from projectdjango.views import page5post
from projectdjango.views import page5schedule
from projectdjango.views import deleteSchedule




urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path(r'^lab-1/', include('lab_1.urls')),
    re_path(r'^lab-2/', include('lab_2.urls')),
    re_path(r'^$lab', index_lab1, name='index'),
    re_path(r'^$', page1, name='page1'),
    re_path(r'^page1', page1, name='page1'),
    re_path(r'^page2', page2, name='page2'),
    re_path(r'^page3', page3, name='page3'),
    re_path(r'^page4', page4, name='page4'),
    re_path(r'^page5form', page5form, name='page5form'),
    re_path(r'^page5post', page5post, name='page5post'),
    re_path(r'^page5schedule', page5schedule, name='page5schedule'),
    re_path(r'^deleteSchedule', deleteSchedule, name='deleteSchedule'),
    
  
]


